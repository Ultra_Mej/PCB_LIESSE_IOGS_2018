
#define phA 8
#define phB 9
#define phC 10
#define LED 11

//rotation duration [ms]
unsigned long  D_max = 250;
unsigned long  D = 250;
unsigned long  D_min = 10;
unsigned long dd = 1000;

unsigned long loop_cnt=0;
long t_0 = micros();

void setup() {

  Serial.begin(9600);
  delay(1000);

  pinMode(phA, OUTPUT);
  pinMode(phB, OUTPUT);
  pinMode(phC, OUTPUT);
  pinMode(LED, OUTPUT);

  digitalWrite(phA, LOW);
  digitalWrite(phB, LOW);
  digitalWrite(phC, LOW);
  digitalWrite(LED, LOW);
  
}

void loop() {

loop_cnt = loop_cnt+1;
long t = micros();

//clignote LED une fois sur deux
if(loop_cnt%2==0){
  digitalWrite(LED, HIGH);}
else{
  digitalWrite(LED, LOW);}


//Update delay
D = floor(D*0.8);
if(D<D_min){
  D = D_max;
  Serial.println(" ");
  }
  
unsigned long dd_hold=dd;
dd = D*1000/12;  //*1000/12 delay of step (1/12th) in us


//Info print
Serial.print("#");
Serial.print(loop_cnt);
Serial.print("\tt= ");
Serial.print((t-t_0)/1000000.0,1);
Serial.print(" s\tD= ");
Serial.print(D);
Serial.print(" ms\tdd= ");
Serial.print(dd);
Serial.print(" us\tRPM= ");
Serial.println(1000.0/D*60,1);

//WHILE loop


int n=0; // pas (us) de translation
int T=1; //nombre de tour à dd_hold-n
// while((micros()-t)<=2000000){ //pendant au moins 2s
while((dd_hold-n)>dd){ //tant que l'on a pas atteind la vitesse target

if(dd>2000){
  n = n+dd/100;
  T=1;
  }
else{
  n = n+10;
  T = 5;
  }




/*
/////////////////////////fait un tour "classique
for(int i=0; i<4*T; i++){
  myWait(dd);
  //delayMicroseconds(dd);
  //delay(D/12);
  digitalWrite(phA, HIGH);
  digitalWrite(phB, LOW);
  digitalWrite(phC, LOW);

  myWait(dd);
  //delayMicroseconds(dd);
  //delay(D/12);
  digitalWrite(phA, LOW);
  digitalWrite(phB, HIGH);
  digitalWrite(phC, LOW);

  myWait(dd);
  //delayMicroseconds(dd);
  //delay(D/12);
  digitalWrite(phA, LOW);
  digitalWrite(phB, LOW);
  digitalWrite(phC, HIGH);
}
*/

/////////////////////////fait un tour "semi-sin"
for(int i=0; i<2*T; i++){
  
  myWait(dd_hold-n);
  digitalWrite(phA, HIGH);
  digitalWrite(phB, LOW);
  digitalWrite(phC, LOW);

  myWait(dd_hold-n);
  digitalWrite(phA, HIGH);
  digitalWrite(phB, HIGH);
  digitalWrite(phC, LOW);
  
  myWait(dd_hold-n);
  digitalWrite(phA, LOW);
  digitalWrite(phB, HIGH);
  digitalWrite(phC, LOW);

  myWait(dd_hold-n);
  digitalWrite(phA, LOW);
  digitalWrite(phB, HIGH);
  digitalWrite(phC, HIGH);
  
  myWait(dd_hold-n);
  digitalWrite(phA, LOW);
  digitalWrite(phB, LOW);
  digitalWrite(phC, HIGH);

  myWait(dd_hold-n);
  digitalWrite(phA, HIGH);
  digitalWrite(phB, LOW);
  digitalWrite(phC, HIGH);
}



}

}

int myWait(unsigned long x){
  if(x>10000){
    delay(x/1000);
    delayMicroseconds(x%1000);
    return 1;
    }
  else{
    delayMicroseconds(x);
    return 2;
    }
}

