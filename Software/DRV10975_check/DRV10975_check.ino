// --------------------------------------
// DRV10975 Arduino driver
//
// Le 24 Avril 2018 by Mej
     
#include <Wire.h>

//## DRV10975 (BLDC motor driver)
//Address
const byte DRV10975 = B1010010; // 0x52  D82  fix address (see datasheet p42)
//Registers
const byte DRV10975_SPEED1 = 0x00;
const byte DRV10975_SPEED2 = 0x01;
const byte DRV10975_STATUS = 0x10;
const byte DRV10975_Real_speed1 = 0x11;
const byte DRV10975_Real_speed2 = 0x12;
const byte DRV10975_FAULT = 0x1E;
//SPEED registry  build up
int DRV10975_speed = 0; //Codé sur 9 bit; entre 0 et 511
bool indent =1; //if 1 speed increases between loops, if 0 speed devreases
byte DRV10975_speed1 = 0;
byte DRV10975_speed2 = B10000000;


void setup()
{
  Wire.begin();
  Serial.begin(9600);
  delay(100);
  while (!Serial);             // wait for serial monitor
  Serial.println("\nI2C is up at 9600 baud");

  Serial.print("\nDRV10975= 0x");
  Serial.println(DRV10975, HEX);


  //Read + display Rm and Kt in EEPROM
  Wire.beginTransmission(DRV10975);
  Wire.write(0x20); //Rm registre
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1); //read Rm registre
  byte Rm = Wire.read();
  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(0x21); //Kt registre
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1); //read Kt registre
  byte Kt = Wire.read();
  delay(1);
  Serial.print("Rm = ");
  Serial.print(Rm, BIN);
  Serial.print("\t Kt = ");
  Serial.print(Kt, BIN);


//  //Writte Rm and Kt in EEPROM
//  Serial.println("\tUpdating EEPROM...");
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x03); //EECtrl register
//  Wire.write(B11000000); //SIdata=1
//  Wire.endTransmission();
//  
//  delay(1);
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x20); //Rm registre
//  Wire.write(B01001101);
//  Wire.endTransmission();
//  delay(1);
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x21); //Kt registre
//  Wire.write(B00011100);
//  Wire.endTransmission();
//  delay(1);
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x02); //DevCtrl registre
//  Wire.write(B10110110); // enProgKey (to allow EEPROM flash)
//  Wire.endTransmission();
//
//  delay(1);
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x03); //EECtrl register
//  Wire.write(B10010000); //eeWrite=1
//  Wire.endTransmission();
//  delay(30); // writting in EEPROM takes 24ms
  
  //Refresh, Read + display Rm and Kt in EEPROM
//  Serial.println("\tRefresh register from EEPROM...");
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x03); //EECtrl register
//  Wire.write(B10100000); //eeRefresh=1
//  Wire.endTransmission();
//  delay(1);
//
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x20); //Rm registre
//  Wire.endTransmission();
//  Wire.requestFrom(DRV10975, 1); //read Rm registre
//  Rm = Wire.read();
//  delay(1);
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x21); //Kt registre
//  Wire.endTransmission();
//  Wire.requestFrom(DRV10975, 1); //read Kt registre
//  Kt = Wire.read();
//  Serial.print("Rm = ");
//  Serial.print(Rm, BIN);
//  Serial.print("\t Kt = ");
//  Serial.println(Kt, BIN);


  //Set Motor speed
  DRV10975_speed = 150; //Codé sur 9 bit; entre 0 et 511
  DRV10975_speed1 = lowByte(DRV10975_speed);
  DRV10975_speed2 = 128 + bitRead(DRV10975_speed, 8); //DRV in "I2C speed control" mode
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_SPEED2);
  Wire.write(DRV10975_speed2);
  Wire.endTransmission(true);
  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_SPEED1);
  Wire.write(DRV10975_speed1);
  Wire.endTransmission(true);

  Serial.print("\nDRV10975_speed= ");
  Serial.print(DRV10975_speed, DEC);
  Serial.println("/512");
  delay(1000);
  
  
}
 
 
void loop()
{
  //Update speed
  if(indent){
    DRV10975_speed = DRV10975_speed+4;}
  else{
    DRV10975_speed = DRV10975_speed-4;}

  //Speed max?
  if(DRV10975_speed>511){
    DRV10975_speed = 511;
    indent = 0;
  }

  //Speed min?
  if(DRV10975_speed<100){
    DRV10975_speed = 100;
    indent = 1;
  }

  //DRV10975_speed = 511;

  //Set Motor
  DRV10975_speed1 = lowByte(DRV10975_speed);
  DRV10975_speed2 = 128 + bitRead(DRV10975_speed, 8); //DRV in "I2C speed control" mode
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_SPEED2);
  Wire.write(DRV10975_speed2);
  Wire.endTransmission();
  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_SPEED1);
  Wire.write(DRV10975_speed1);
  Wire.endTransmission();

  //Read Motor S1 and S2 register, Status, Faultand DRV_Real_speed
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_SPEED1);
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1); //read 1 bytes
  byte S1 = Wire.read();

  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_SPEED2);
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1); //read 1 bytes
  byte S2 = Wire.read();

  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_STATUS);
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1); //read 1 bytes
  byte DRV10975_status = Wire.read()/16; //Only the first 4 bits matter

  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_FAULT);
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1); //read 1 bytes
  byte DRV10975_fault = Wire.read(); //Only the last 6 bits matter
  
  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_Real_speed1);
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1); //read 1 bytes
  unsigned int DRV_Real_speed = Wire.read();
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_Real_speed2);
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1);
  DRV_Real_speed = DRV_Real_speed*256+ Wire.read();


  //Print and wait
  Serial.print("\nDRV10975_speed= ");
  Serial.print(DRV10975_speed, DEC);
  Serial.print("/512");
  Serial.print("  \t Status= ");
  Serial.print(DRV10975_status, BIN);
  Serial.print(" \tFault= ");
  Serial.println(DRV10975_fault, BIN);
  
  Serial.print("DRV10975_speed2= ");
  Serial.print(DRV10975_speed2,BIN);
  Serial.print("\tDRV10975_speed1= ");
  Serial.println(DRV10975_speed1,DEC);
  
  Serial.print("DRV_Checkspeed2= ");
  Serial.print(S2,BIN);
  Serial.print("\tDRV_Checkspeed1= ");
  Serial.println(S1,DEC);
  Serial.print("DRV_Real_speed = ");
  Serial.println(DRV_Real_speed,DEC);
  delay(1000);
  
}

