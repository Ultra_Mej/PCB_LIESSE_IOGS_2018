// --------------------------------------
// DRV8830 Arduino driver
//
// Le 27 Avril 2018 by Mej
     


/////////////////////////////////////////////////////////////////////////////////////////////
//## DRV8830 (H-Bridge Voltage-Controlled Motor Driver)

//Addresses                                                 #IC    pin A0  A1       Outputs
const byte DRV8830_0 = B1100100; // 0x64   adress=110+ 0100   3      Open    Open   3+/3- for DC motor
const byte DRV8830_1 = B1100011; // 0x63   adress=110+ 0011   1      GND     Open   1+/1- for 1st coil of Stepper-Motor
const byte DRV8830_2 = B1100001; // 0x61   adress=110+ 0001   2      Open    GND    2+/2- for 2nd coil of Stepper-Motor
const byte DRV8830 = DRV8830_0;
const byte DRV8830s[3] = {DRV8830_0, DRV8830_1, DRV8830_2};

//Registers
const byte DRV8830_CONTROL = 0x00;
const byte DRV8830_FAULT = 0x01;

//CONTROL registry  tools
const byte DRV8830_VSETmax = B111111; // 0x3F D63 // Output voltage = 5.06V (PWM controled)
const byte DRV8830_VSETmin = B000110; // 0x06 D6 // Output voltage = 0.48V (PWM controled)
byte DRV8830s_VSET[3] = {DRV8830_VSETmax/2, DRV8830_VSETmax/2, DRV8830_VSETmax/2}; //PWM average voltage
bool DRV8830s_IN1[3] = {1,0,0}; //H-Brigdes logic
bool DRV8830s_IN2[3] = {0,0,0};
byte DRV8830s_control[3]={0,0,0}; //Value to be loaded in register

//FAULT registry tools
const byte DRV8830_fault_clear = B10000000; //0x80   used to clear FAULT
byte DRV8830_fault; //used to store FAULT register


/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Other variables

//timing
unsigned long t_0 = millis(); //Time since startup in ms. Overflow after 50 days.
unsigned long t = millis();
unsigned long loop_cnt = 0; //loop count

//used to increase or decrease speed of DRV 0 (3 on PCB)
bool indent=1;

////Steping sequence 1: "Wave drive"
//const bool Sequence_L1_P1[4] = {1, 0, 0, 0}; //1st Coil, 1st Phase
//const bool Sequence_L1_P2[4] = {0, 0, 1, 0}; //1st Coil, 2nd Phase
//const bool Sequence_L2_P1[4] = {0, 1, 0, 0}; //2nd Coil, 1st Phase
//const bool Sequence_L2_P2[4] = {0, 0, 0, 1}; //2nd Coil, 2nd Phase
//const byte Under_step = 4; //Number of steps in "one step"

//Steping sequence 2: "Full step"
const bool Sequence_L1_P1[4] = {1, 1, 0, 0}; //1st Coil, 1st Phase
const bool Sequence_L1_P2[4] = {0, 0, 1, 1}; //1st Coil, 2nd Phase
const bool Sequence_L2_P1[4] = {0, 1, 1, 0}; //2nd Coil, 1st Phase
const bool Sequence_L2_P2[4] = {1, 0, 0, 1}; //2nd Coil, 2nd Phase
const byte Under_step = 4; //Number of steps in "one step"


//////Steping sequence 2: "Half step"
//const bool Sequence_L1_P1[8] = {1, 1, 0, 0, 0, 0, 0, 1}; //1st Coil, 1st Phase
//const bool Sequence_L1_P2[8] = {0, 0, 0, 1, 1, 1, 0, 0}; //1st Coil, 2nd Phase
//const bool Sequence_L2_P1[8] = {0, 1, 1, 1, 0, 0, 0, 0}; //2nd Coil, 1st Phase
//const bool Sequence_L2_P2[8] = {0, 0, 0, 0, 0, 1, 1, 1}; //2nd Coil, 2nd Phase
//const byte Under_step = 8; //Number of steps in "one step"

void set_Step(byte STEP)
{
  STEP = STEP%Under_step;
  DRV8830s_IN1[1] = Sequence_L1_P1[STEP];
  DRV8830s_IN2[1] = Sequence_L1_P2[STEP];
  DRV8830s_IN1[2] = Sequence_L2_P1[STEP];
  DRV8830s_IN2[2] = Sequence_L2_P2[STEP];
}





/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Wire and functions

#include <Wire.h>

void set_I2C_register(byte ADDRESS, byte REGISTER, byte VALUE)
{
  Wire.beginTransmission(ADDRESS);
  Wire.write(REGISTER);
  Wire.write(VALUE);
  Wire.endTransmission();
}

byte get_I2C_register(byte ADDRESS, byte REGISTER)
{
  Wire.beginTransmission(ADDRESS);
  Wire.write(REGISTER);
  Wire.endTransmission();
  Wire.requestFrom(ADDRESS, 1); //read 1 byte
  byte x = Wire.read();
  return x;
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////  SETUP
// 
void setup()
{
  Wire.begin();
  Serial.begin(9600);
  delay(100);
  while (!Serial);             // wait for serial monitor
  Serial.println("\n");
  Serial.println("\nI2C is up at 9600 baud");
  Serial.print("\n");
  for (int i=0; i < 3; i++)
  {
    Serial.print("DRV8830_");
    Serial.print(i);
    Serial.print(" address = 0x");
    Serial.print(DRV8830s[i], HEX);
    Serial.print(" = B");
    Serial.println(DRV8830s[i], BIN);
  }


  //Clear all DRV8830 and set them at DRV8830_VSETmin
  for (int i=0; i < 3; i++)
  {
    set_I2C_register(DRV8830s[i], DRV8830_CONTROL, 4*DRV8830_VSETmin + 2*0 + 0); // Standby/coast
    //set_I2C_register(DRV8830s[i], DRV8830_CONTROL, 4*DRV8830_VSETmin + 2*1 + 1); // Break
    set_I2C_register(DRV8830s[i], DRV8830_FAULT, DRV8830_fault_clear);           //Clear fault
  }
  delay(100);


  //Choose configurations of DC motor, drived by DRV8830_0 on connector 3+/3-
  DRV8830s_IN1[0] = 1;
  DRV8830s_IN2[0] = 0;
  //DRV8830s_VSET[0] = DRV8830_VSETmax/4;


  //Choose configurations of DC motor, drived by DRV8830_0 on connector 3+/3-
  set_Step(0);
  //DRV8830s_VSET[1] = DRV8830_VSETmax/2;
  //DRV8830s_VSET[2] = DRV8830_VSETmax/2;



  //Set Motor
  Serial.print("\n");
  Serial.print("DRV8830s_control=");
  for (int i=0; i < 3; i++)
  {
    DRV8830s_control[i] = 4*DRV8830s_VSET[i] + 2*DRV8830s_IN2[i] + DRV8830s_IN1[i];
    set_I2C_register(DRV8830s[i], DRV8830_CONTROL, DRV8830s_control[i]);
    Serial.print("\t");
    Serial.print(DRV8830s_control[i], HEX);
  }
  delay(1000);


  //Read and print CONTROL registers
  Serial.print("\nDRV8830s_control=");
  for (int i=0; i < 3; i++)
  {
    Serial.print("\t");
    Serial.print(get_I2C_register(DRV8830s[i], DRV8830_CONTROL), HEX);
  }
  Serial.print(" (checked from register)");

  
  //Read and print FAULT registers (see Table 8 of datasheet) of all DRV8830s
  Serial.print("\n");
  for (int i=0; i < 3; i++)
  {
    DRV8830_fault = get_I2C_register(DRV8830s[i], DRV8830_FAULT);
    Serial.print("\t");
    Serial.print("\nDRV8830s_fault=");
    Serial.print(DRV8830_fault, BIN);
    if (bitRead(DRV8830_fault,0)==1)               //D0: FAULT
    {
      Serial.print("faulty:");
      if (bitRead(DRV8830_fault,1)==1){             //D1: OCP
        Serial.print("  OCP = Overcurrent event!");}
      else if (bitRead(DRV8830_fault,2)==1){        //D2: UVLO
        Serial.print("  UVLO = Undervoltage lockout!");}
      else if (bitRead(DRV8830_fault,3)==1){        //D3: OTS
        Serial.print("  OTS = Overtemperature condition!");}
      else if (bitRead(DRV8830_fault,4)==1){        //D4: ILIMIT
        Serial.print("  OTS = extended current limit event!");}
    }
    else
    {
      Serial.print(" OK");
    }
  }
  Serial.println("\n");
}
 


/////////////////////////////////////////////////////////////////////////////////////////////////////////  LOOP
// 
void loop()
{
  loop_cnt = loop_cnt+1;
  t = millis();
  Serial.print("\nt= ");
  Serial.print(t/1000.0, 3);
  Serial.print("s");

  //Update VSET of DC motor
  if(indent){
    DRV8830s_VSET[0] = DRV8830s_VSET[0]+1;}
  else{
    DRV8830s_VSET[0] = DRV8830s_VSET[0]-1;}
  //VSET max?
  if(DRV8830s_VSET[0]>DRV8830_VSETmax){
    DRV8830s_VSET[0] = DRV8830_VSETmax;
    indent = 0;   //Speed will now decrease
    //delay(1000); //Staty 1s at max speed
  }
  //VSET min?
  if(DRV8830s_VSET[0]<DRV8830_VSETmin){
    DRV8830s_VSET[0] = DRV8830_VSETmin;
    indent = 1;  //Speed will now increase and direction will change
    DRV8830s_IN1[0] = !DRV8830s_IN1[0];
    DRV8830s_IN2[0] = !DRV8830s_IN2[0];
  }


  //Update Stepper motor
  set_Step(loop_cnt%Under_step);
  

  //Print time VSET et IN1-IN2 config (Direction, Break, Free)
  Serial.print("\tVSET=");
  for (int i=0; i < 3; i++)
  {
    Serial.print("\t");
    Serial.print(DRV8830s_VSET[i]);
  }
  Serial.print("\tIN12=  ");
  Serial.print(DRV8830s_IN1[0]);
  Serial.print(DRV8830s_IN2[0]);
  Serial.print("   ");
  Serial.print(DRV8830s_IN1[1]);
  Serial.print(DRV8830s_IN2[1]);
  Serial.print(" ");
  Serial.print(DRV8830s_IN1[2]);
  Serial.print(DRV8830s_IN2[2]);
  
    
  //Set all DRV8830
  for (int i=0; i < 3; i++){
    DRV8830s_control[i] = 4*DRV8830s_VSET[i] + 2*DRV8830s_IN2[i] + DRV8830s_IN1[i];
    set_I2C_register(DRV8830s[i], DRV8830_CONTROL, DRV8830s_control[i]);
  }


  //Read CONTROL register and check is has expected value
  Serial.print("  CONTROL: ");
  for (int i=0; i < 3; i++){
    byte DRV8830_control_check = get_I2C_register(DRV8830s[i], DRV8830_CONTROL);
    if (DRV8830_control_check == DRV8830s_control[i]){
      Serial.print("ok ");}
    else{
      Serial.print("!!! CONTROL of DRV8830_");
      Serial.print(i);
      Serial.print(" is ");
      Serial.print(DRV8830_control_check,BIN);
      Serial.print(" instead of ");
      Serial.print(DRV8830s_control[i] ,BIN);
    }
  }


  //Check FAULT register
  Serial.print("  FAULT: ");
  for (int i=0; i < 3; i++)
  {
    DRV8830_fault = get_I2C_register(DRV8830s[i], DRV8830_FAULT);
    if (bitRead(DRV8830_fault,0)==1)               //D0: FAULT
    {
      Serial.print("\nDRV8830_");
      Serial.print(i);
      Serial.print(" is faulty:");
      if (bitRead(DRV8830_fault,1)==1){             //D1: OCP
        Serial.print("  OCP = Overcurrent event!");}
      else if (bitRead(DRV8830_fault,2)==1){        //D2: UVLO
        Serial.print("  UVLO = Undervoltage lockout!");}
      else if (bitRead(DRV8830_fault,3)==1){        //D3: OTS
        Serial.print("  OTS = Overtemperature condition!");}
      else if (bitRead(DRV8830_fault,4)==1){        //D4: ILIMIT
        Serial.print("  OTS = extended current limit event!");}
    }
    else
    {
      //Serial.print("ok ");
      
      Serial.print("\t");
      Serial.print(i);
      Serial.print(":");
      Serial.print(DRV8830_fault,BIN);
    }
  }
    
  delay(100);
  
}

