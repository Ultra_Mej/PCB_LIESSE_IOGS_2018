// --------------------------------------
// DRV10975 Arduino driver
//
// Le 24 Avril 2018 by Mej
     
#include <Wire.h>

//## DRV10975 (BLDC motor driver)
//Address
const byte DRV10975 = B1010010; // 0x52  D82  fix address (see datasheet p42)
//Ctrl Registers (read/writte)
const byte DRV10975_SPEED1 = 0x00;
const byte DRV10975_SPEED2 = 0x01;
//Status Registers (read only)
const byte DRV10975_STATUS = 0x10;
const byte DRV10975_FAULT = 0x1E;
const byte DRV10975_Real_speed1 = 0x11;
const byte DRV10975_Real_speed2 = 0x12;
//SPEED registry  build up
int DRV10975_speed = 0; //Codé sur 9 bit; entre 0 et 511
bool indent =1; //if 1 speed increases between loops, if 0 speed devreases
byte DRV10975_speed1 = 0;
byte DRV10975_speed2 = B10000000;


void set_I2C_register(byte ADDRESS, byte REGISTER, byte VALUE)
{
  Wire.beginTransmission(ADDRESS);
  Wire.write(REGISTER);
  Wire.write(VALUE);
  Wire.endTransmission();
}


byte get_I2C_register(byte ADDRESS, byte REGISTER)
{
  Wire.beginTransmission(ADDRESS);
  Wire.write(REGISTER);
  Wire.endTransmission();
  Wire.requestFrom(ADDRESS, 1); //read 1 byte
  byte x = Wire.read();
  return x;
}



void setup()
{
  Wire.begin();
  Serial.begin(9600);
  delay(100);
  while (!Serial);             // wait for serial monitor
  Serial.println("\nI2C is up at 9600 baud");
  Serial.print("\nDRV10975 address = 0x");
  Serial.print(DRV10975, HEX);
  Serial.print(" = B");
  Serial.println(DRV10975, BIN);


  ////////////////////////////////////////////////////////////////////////////////
  //Read and print DRV10975 status registers
  
  //Status
  byte Status = get_I2C_register(DRV10975, 0x10);
  Serial.print("\nDRV10975 Status = 0x");
  Serial.print(Status, HEX);
  Serial.print("\t=>");
  if (bitRead(Status,4)==1){
    Serial.print("\tMtrLck = Motor is locked!");}
  if (bitRead(Status,5)==1){
    Serial.print("\tOverCurr = Over current event!");}
  if (bitRead(Status,6)==1){
    Serial.print("\tSlp_Stdby = DRV is into sleep or standby mode!");}
  if (bitRead(Status,7)==1){
    Serial.print("\tOverTemp = DRV temperature is over its limit!");}
  if (bitRead(Status,4)+bitRead(Status,5)+bitRead(Status,6)+bitRead(Status,7)==0){
    Serial.print("\t = RAS");}
 

  //Motor_Speed
  byte Motor_Speed1 = get_I2C_register(DRV10975, 0x11);
  byte Motor_Speed2 = get_I2C_register(DRV10975, 0x12);
  unsigned int Motor_Speed = Motor_Speed1*256 + Motor_Speed2;
  Serial.print("\nDRV10975 Motor_Speed = 0x");
  Serial.print(Motor_Speed, HEX);
  Serial.print(" = ");
  Serial.print(Motor_Speed/10.0, 1);
  Serial.print(" Hz");

  //Motor_Period
  byte Motor_Period1 = get_I2C_register(DRV10975, 0x13);
  byte Motor_Period2 = get_I2C_register(DRV10975, 0x14);
  unsigned int Motor_Period = Motor_Period1*256 + Motor_Period2;
  Serial.print("\nDRV10975 Motor_Period = 0x");
  Serial.print(Motor_Period, HEX);
  Serial.print(" = ");
  Serial.print(Motor_Period*10, DEC);
  Serial.print(" us");

  //Motor_Kt
  byte Motor_Kt1 = get_I2C_register(DRV10975, 0x15);
  byte Motor_Kt2 = get_I2C_register(DRV10975, 0x16);
  unsigned int Motor_Kt = Motor_Kt1*256 + Motor_Kt2;
  Serial.print("\nDRV10975 Motor_Kt = 0x");
  Serial.print(Motor_Kt, HEX);
  Serial.print(" = ");
  Serial.print(Motor_Kt/2./1090, 3);
  Serial.print(" V/Hz");

  //IPDPosition
  byte IPDPosition = get_I2C_register(DRV10975, 0x19);
  Serial.print("\nDRV10975 IPDPosition = 0x");
  Serial.print(IPDPosition, HEX); //see details in Table7

  //Supply_voltage
  byte Supply_voltage = get_I2C_register(DRV10975, 0x1A);
  Serial.print("\nDRV10975 Supply_voltage = 0x");
  Serial.print(Supply_voltage, HEX);
  Serial.print(" = ");
  Serial.print(Supply_voltage*22.8/256, 2);
  Serial.print("V");

  //SpeedCmd
  byte SpeedCmd = get_I2C_register(DRV10975, 0x1B);
  Serial.print("\nDRV10975 SpeedCmd = 0x");
  Serial.print(SpeedCmd, HEX);
  Serial.print(" = ");
  Serial.print(SpeedCmd*100.0/256, 1);
  Serial.print("%");

  //SpeedCmdBuffer
  byte SpeedCmdBuffer = get_I2C_register(DRV10975, 0x1C);
  Serial.print("\nDRV10975 SpeedCmdBuffer = 0x");
  Serial.print(SpeedCmdBuffer, HEX);
  Serial.print(" = ");
  Serial.print(SpeedCmdBuffer*100.0/256, 1);
  Serial.print("%");

  //FaultCode
  byte FaultCode = get_I2C_register(DRV10975, 0x1E);
  Serial.print("\nDRV10975 FaultCode = 0x");
  Serial.print(FaultCode, HEX);
  Serial.print("\t=>");
  if (bitRead(FaultCode,0)==1){
    Serial.print("\tLock0 = Lock detection current limit!");}
  if (bitRead(FaultCode,1)==1){
    Serial.print("\tLock1 = Speed abnormal!");}
  if (bitRead(FaultCode,2)==1){
    Serial.print("\tLock2 = Kt abnormal!");}
  if (bitRead(FaultCode,3)==1){
    Serial.print("\tLock3 = No motor!");}
  if (bitRead(FaultCode,4)==1){
    Serial.print("\tLock4 = Stuck in open loop!");}
  if (bitRead(FaultCode,5)==1){
    Serial.print("\tLock5 = Stuck in closed loop!");}
  if (bitRead(FaultCode,0)+bitRead(FaultCode,1)+bitRead(FaultCode,2)+bitRead(FaultCode,3)+bitRead(FaultCode,4)+bitRead(FaultCode,5)==0){
    Serial.print("\t = RAS");}

  //Read + display Rm and Kt in EEPROM
  Serial.println("");
  Serial.println("\nEEPROM:");
  byte Rm = get_I2C_register(DRV10975, 0x10); //Rm registre
  Serial.print("\tRm = ");
  Serial.println(Rm, BIN);
  byte Kt = get_I2C_register(DRV10975, 0x21); //Kt registre
  Serial.print("\tKt = ");
  Serial.println(Kt, BIN);


//  //Writte Rm and Kt in EEPROM
//  Serial.println("\tUpdating EEPROM...");
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x03); //EECtrl register
//  Wire.write(B11000000); //SIdata=1
//  Wire.endTransmission();
//  
//  delay(1);
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x20); //Rm registre
//  Wire.write(B01001101);
//  Wire.endTransmission();
//  delay(1);
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x21); //Kt registre
//  Wire.write(B00011100);
//  Wire.endTransmission();
//  delay(1);
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x02); //DevCtrl registre
//  Wire.write(B10110110); // enProgKey (to allow EEPROM flash)
//  Wire.endTransmission();
//
//  delay(1);
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x03); //EECtrl register
//  Wire.write(B10010000); //eeWrite=1
//  Wire.endTransmission();
//  delay(30); // writting in EEPROM takes 24ms
  
  //Refresh  EEPROM
//  Serial.println("\tRefresh register from EEPROM...");
//  Wire.beginTransmission(DRV10975);
//  Wire.write(0x03); //EECtrl register
//  Wire.write(B10100000); //eeRefresh=1
//  Wire.endTransmission();
//  delay(1);

  //Set Motor speed
  DRV10975_speed = 150; //Codé sur 9 bit; entre 0 et 511
  DRV10975_speed1 = lowByte(DRV10975_speed);
  DRV10975_speed2 = 128 + bitRead(DRV10975_speed, 8); //DRV in "I2C speed control" mode
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_SPEED2);
  Wire.write(DRV10975_speed2);
  Wire.endTransmission(true);
  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_SPEED1);
  Wire.write(DRV10975_speed1);
  Wire.endTransmission(true);

  Serial.print("\nDRV10975_speed= ");
  Serial.print(DRV10975_speed, DEC);
  Serial.println("/512");
  delay(1000);
  
  
}
 
 
void loop()
{
  //Update speed
  if(indent){
    DRV10975_speed = DRV10975_speed+4;}
  else{
    DRV10975_speed = DRV10975_speed-4;}

  //Speed max?
  if(DRV10975_speed>511){
    DRV10975_speed = 511;
    indent = 0;
  }

  //Speed min?
  if(DRV10975_speed<100){
    DRV10975_speed = 100;
    indent = 1;
  }

  DRV10975_speed = 511;

  //Set Motor
  DRV10975_speed1 = lowByte(DRV10975_speed);
  DRV10975_speed2 = 128 + bitRead(DRV10975_speed, 8); //DRV in "I2C speed control" mode
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_SPEED2);
  Wire.write(DRV10975_speed2);
  Wire.endTransmission();
  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_SPEED1);
  Wire.write(DRV10975_speed1);
  Wire.endTransmission();

  //Read Motor S1 and S2 register, Status, Faultand DRV_Real_speed
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_SPEED1);
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1); //read 1 bytes
  byte S1 = Wire.read();

  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_SPEED2);
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1); //read 1 bytes
  byte S2 = Wire.read();

  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_STATUS);
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1); //read 1 bytes
  byte DRV10975_status = Wire.read()/16; //Only the first 4 bits matter

  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_FAULT);
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1); //read 1 bytes
  byte DRV10975_fault = Wire.read(); //Only the last 6 bits matter
  
  delay(1);
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_Real_speed1);
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1); //read 1 bytes
  unsigned int DRV_Real_speed = Wire.read();
  Wire.beginTransmission(DRV10975);
  Wire.write(DRV10975_Real_speed2);
  Wire.endTransmission();
  Wire.requestFrom(DRV10975, 1);
  DRV_Real_speed = DRV_Real_speed*256+ Wire.read();


  //Print and wait
  Serial.print("\nDRV10975_speed= ");
  Serial.print(DRV10975_speed, DEC);
  Serial.print("/512");
  Serial.print("  \t Status= ");
  Serial.print(DRV10975_status, BIN);
  Serial.print(" \tFault= ");
  Serial.println(DRV10975_fault, BIN);
  
  Serial.print("DRV10975_speed2= ");
  Serial.print(DRV10975_speed2,BIN);
  Serial.print("\tDRV10975_speed1= ");
  Serial.println(DRV10975_speed1,DEC);
  
  Serial.print("DRV_Checkspeed2= ");
  Serial.print(S2,BIN);
  Serial.print("\tDRV_Checkspeed1= ");
  Serial.println(S1,DEC);
  Serial.print("DRV_Real_speed = ");
  Serial.println(DRV_Real_speed,DEC);
  delay(1000);
  
}

