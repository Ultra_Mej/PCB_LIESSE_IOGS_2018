// --------------------------------------
// DRV8830 Arduino driver
//
// Le 22 Avril 2018 by Mej
     
#include <Wire.h>

//## DRV8830 (BDC motor driver)
//Address                                                   #IC    pin A0  A1
const byte DRV8830 = B1100100; // 0x64   adress=110+ 0100   3      Open    Open       
//const byte DRV8830 = B1100001; // 0x61   adress=110+ 0001   2      Open    GND
//const byte DRV8830 = B1100011; // 0x63   adress=110+ 0011   1      GND     Open
//Registers
const byte DRV8830_CONTROL = 0x00;
const byte DRV8830_FAULT = 0x01;
//CONTROL registry  build up
const byte DRV8830_VSETmax = B110010; // 0x32 D50 // Output voltage = 4.02V (PWM controled)
const byte DRV8830_VSETmin = B000110; // 0x06 D6 // Output voltage = 0.48V (PWM controled)
byte DRV8830_VSET = 0;
bool DRV8830_IN1 = 1;
bool DRV8830_IN2 = 0;
byte DRV8830_control = 4*DRV8830_VSET + 2*DRV8830_IN2 + DRV8830_IN1;
//FAULT clear
byte DRV8830_fault = B10000000; //0x80
//
bool indent=1; //used to increase or decrease speed of motor

void setup()
{
  Wire.begin();
  Serial.begin(9600);
  delay(100);
  while (!Serial);             // wait for serial monitor
  Serial.println("\nI2C is up");

  Serial.print("\nDRV8830= ");
  Serial.println(DRV8830, HEX);
  Serial.print("\DRV8830_control= ");
  Serial.println(DRV8830_control, BIN);

  //DRV in "stand-by / coast mode"
  DRV8830_IN1 = 1;
  DRV8830_IN2 = 0;
  DRV8830_VSET = 0;

  //Set Motor
  DRV8830_control = 4*DRV8830_VSET + 2*DRV8830_IN2 + DRV8830_IN1;
  Wire.beginTransmission(DRV8830);
  Wire.write(DRV8830_CONTROL);
  Wire.write(DRV8830_control);
  Wire.endTransmission(true);

  //Clear Motor
  Wire.beginTransmission(DRV8830);
  Wire.write(DRV8830_FAULT);
  Wire.write(DRV8830_fault);
  Wire.endTransmission(true);
  
}
 
 
void loop()
{
  //Update VSET
  if(indent){
    DRV8830_VSET = DRV8830_VSET+1;}
  else{
    DRV8830_VSET = DRV8830_VSET-1;}


  if(DRV8830_VSET>DRV8830_VSETmax){
    DRV8830_VSET = DRV8830_VSETmax;
    indent = 0;
     delay(1000);

//    Serial.println(" ");
//    Serial.println("!! Changing direction !!");
//    DRV8830_IN1 = !DRV8830_IN1;
//    DRV8830_IN2 = !DRV8830_IN2;
//    Serial.print("IN1=");
//    Serial.println(DRV8830_IN1);
//    Serial.print("IN2=");
//    Serial.println(DRV8830_IN2);
//    Serial.println(" ");
  }

  if(DRV8830_VSET<DRV8830_VSETmin){
    DRV8830_VSET = DRV8830_VSETmin;
    indent = 1;
    Serial.println(" ");
    Serial.println("!! Changing direction !!");
    DRV8830_IN1 = !DRV8830_IN1;
    DRV8830_IN2 = !DRV8830_IN2;
    Serial.print("IN1=");
    Serial.println(DRV8830_IN1);
    Serial.print("IN2=");
    Serial.println(DRV8830_IN2);
    Serial.println(" ");
  }


  //Set Motor
  DRV8830_control = 4*DRV8830_VSET + 2*DRV8830_IN2 + DRV8830_IN1;
  Wire.beginTransmission(DRV8830);
  Wire.write(DRV8830_CONTROL);
  Wire.write(DRV8830_control);
  Wire.endTransmission(true);

  //Print and wait
  Serial.print("VSET=");
  Serial.print(DRV8830_VSET,DEC);
  Serial.print("\tDRV8830_control= ");
  Serial.println(DRV8830_control, BIN);
  delay(10);
  
}

