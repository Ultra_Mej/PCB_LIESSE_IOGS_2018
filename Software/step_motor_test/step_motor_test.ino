//Pin definition
int LED = 3;
int LED_minus = 2;
int A_p = 4;
int A_m = 5;
int B_p = 6;
int B_m = 7;

//Macro 
int D = 5; //Delay en ms
int long loop_count = 0;
int steps_per_cycle=50; //steps before changing direction

void setup() {
// GPIO set up

 pinMode(LED, OUTPUT);
 pinMode(LED_minus, LOW);
 pinMode(A_p, OUTPUT);
 pinMode(A_m, OUTPUT);
 pinMode(B_p, OUTPUT);
 pinMode(B_m, OUTPUT);
 
 digitalWrite(LED, LOW);
 digitalWrite(A_p, LOW);
 digitalWrite(A_m, LOW);
 digitalWrite(B_p, LOW);
 digitalWrite(B_m, LOW);
}

void loop() {

  loop_count = loop_count + 1;
  //D = 5 + int(loop_count/100)%200;
  
  digitalWrite(LED, HIGH);

  if ( loop_count%steps_per_cycle < (steps_per_cycle/2) )
  {

  //step 11 cf http://www.edaboard.com/thread217270.html
  digitalWrite(A_p, HIGH);
  digitalWrite(A_m, LOW);
  digitalWrite(B_p, HIGH);
  digitalWrite(B_m, LOW);
  delay(D);

  //step 10
  digitalWrite(A_p, HIGH);
  digitalWrite(A_m, LOW);
  digitalWrite(B_p, LOW);
  digitalWrite(B_m, HIGH);
  delay(D);

  digitalWrite(LED, LOW);

  //step 00
  digitalWrite(A_p, LOW);
  digitalWrite(A_m, HIGH);
  digitalWrite(B_p, LOW);
  digitalWrite(B_m, HIGH);
  delay(D);

  //step 01
  digitalWrite(A_p, LOW);
  digitalWrite(A_m, HIGH);
  digitalWrite(B_p, HIGH);
  digitalWrite(B_m, LOW);
  delay(D);
  
  }
  else
  {
    
  //step 01
  digitalWrite(A_p, LOW);
  digitalWrite(A_m, HIGH);
  digitalWrite(B_p, HIGH);
  digitalWrite(B_m, LOW);
  delay(D);

  //step 00
  digitalWrite(A_p, LOW);
  digitalWrite(A_m, HIGH);
  digitalWrite(B_p, LOW);
  digitalWrite(B_m, HIGH);
  delay(D);

  digitalWrite(LED, LOW);

  //step 10
  digitalWrite(A_p, HIGH);
  digitalWrite(A_m, LOW);
  digitalWrite(B_p, LOW);
  digitalWrite(B_m, HIGH);
  delay(D);
  
  //step 11
  digitalWrite(A_p, HIGH);
  digitalWrite(A_m, LOW);
  digitalWrite(B_p, HIGH);
  digitalWrite(B_m, LOW);
  delay(D);
  
  }

}
