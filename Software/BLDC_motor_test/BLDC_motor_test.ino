
#define phA 8
#define phB 9
#define phC 10
#define LED 11

//rotation Speed [RPM]
unsigned long  W_max = 1500;
unsigned long  W = 150;
unsigned long  W_min = 150;
unsigned long dd = 60/12*1000000/W;

unsigned long loop_cnt=0;
long t_0 = micros();

void setup() {

  Serial.begin(115200);
  delay(1000);

  pinMode(phA, OUTPUT);
  pinMode(phB, OUTPUT);
  pinMode(phC, OUTPUT);
  pinMode(LED, OUTPUT);

  digitalWrite(phA, LOW);
  digitalWrite(phB, LOW);
  digitalWrite(phC, LOW);
  digitalWrite(LED, LOW);
  
}

void loop() {

loop_cnt = loop_cnt+1;
long t = micros();

//clignote LED une loop sur deux
if(loop_cnt%2==0){
  digitalWrite(LED, HIGH);}
else{
  digitalWrite(LED, LOW);}


//Compute delay: W [RPM] = 60/t_turn[s] = 60/(12*dd)  <=> dd = 60/12/W = 5/W*1e6
unsigned long dd_old=dd;
dd = 60/12*1000000/W;  //delay of step (1/12th of turn) in µs


//Info print
Serial.print("#");
Serial.print(loop_cnt);
Serial.print("\tt= ");
Serial.print((t-t_0)/1000000.0,1);
Serial.print(" s\t W= ");
Serial.print(W);
Serial.print(" RPM\tdd= ");
Serial.print(dd);
Serial.print(" us\tt_turn= ");
Serial.print(dd*12.0/1000,1);
Serial.println(" ms");

//WHILE loop

int n=0;
int T=1;
// while((micros()-t)<=2000000){ //pendant au moins 2s
while((dd_old-n)>dd){ //tant que l'on a pas atteind la vitesse target

if(W<1000){
  n = n+dd*5/100;
  T=1;
  }
else{
  n = n+dd/100;
  T=5;
  }




/*
/////////////////////////fait un tour "classique
for(int i=0; i<4; i++){
  myWait(dd);
  //delayMicroseconds(dd);
  //delay(D/12);
  digitalWrite(phA, HIGH);
  digitalWrite(phB, LOW);
  digitalWrite(phC, LOW);

  myWait(dd);
  //delayMicroseconds(dd);
  //delay(D/12);
  digitalWrite(phA, LOW);
  digitalWrite(phB, HIGH);
  digitalWrite(phC, LOW);

  myWait(dd);
  //delayMicroseconds(dd);
  //delay(D/12);
  digitalWrite(phA, LOW);
  digitalWrite(phB, LOW);
  digitalWrite(phC, HIGH);
}
*/

/////////////////////////fait T tour "semi-sin"
for(int i=0; i<2*T; i++){
  
  myWait(dd_old-n);
  digitalWrite(phA, HIGH);
  digitalWrite(phB, LOW);
  digitalWrite(phC, LOW);

  myWait(dd_old-n);
  digitalWrite(phA, HIGH);
  digitalWrite(phB, HIGH);
  digitalWrite(phC, LOW);
  
  myWait(dd_old-n);
  digitalWrite(phA, LOW);
  digitalWrite(phB, HIGH);
  digitalWrite(phC, LOW);

  myWait(dd_old-n);
  digitalWrite(phA, LOW);
  digitalWrite(phB, HIGH);
  digitalWrite(phC, HIGH);
  
  myWait(dd_old-n);
  digitalWrite(phA, LOW);
  digitalWrite(phB, LOW);
  digitalWrite(phC, HIGH);

  myWait(dd_old-n);
  digitalWrite(phA, HIGH);
  digitalWrite(phB, LOW);
  digitalWrite(phC, HIGH);
}



}

//Update speed
W = floor(W*1.2);
if(W>W_max){
  W = W_min;
  Serial.println(" ");
  }
  
}

int myWait(unsigned long x){
  if(x>10000){
    delay(x/1000);
    delayMicroseconds(x%1000);
    return 1;
    }
  else{
    delayMicroseconds(x);
    return 2;
    }
}

