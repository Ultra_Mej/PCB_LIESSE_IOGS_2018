    // --------------------------------------
    // i2c_scanner
    //
    // Le 8 Decembre 2018 by Mej
    // Copy/Pasted/Tweeked from https://playground.arduino.cc/Main/I2cScanner
     
#include <Wire.h>

//## MCP9800 (9-12bit thermometer) address
//Address
const byte MCP9800_A0 = B1001000; // 0x48
const byte MCP9800_A1 = B1001001; // 0x49
const byte MCP9800_A2 = B1001010; // 0x4A
const byte MCP9800_A3 = B1001011; // 0x4B
const byte MCP9800_A4 = B1001100; // 0x4C
const byte MCP9800_A5 = B1001101; // 0x4D
byte MCP9800 = MCP9800_A0;
const byte MCP9800s[] = {MCP9800_A0, MCP9800_A1, MCP9800_A2, MCP9800_A4};
float Ts[] = {66.6, 66.6, 66.6, 66.6};
const byte MCP9800_cnt = 4;
//Registers
const byte MCP9800_TEMP =       0x00;  // Temperatur register
const byte MCP9800_CONFIG =     0x01;  // Configuration register
const byte MCP9800_HYSTERISIS = 0x02;  // Temperature Hysteresis register
const byte MCP9800_LIMIT =      0x03;  // Temperature Limit-set register
//default config, mais 12bits (cf page 13/31 de la datasheet)
const byte MCP9800_default = B01100000; 

 
void setup()
{
  Wire.begin();
 
  Serial.begin(115200);
  while (!Serial);             // wait for serial monitor
  Serial.println("\nI2C Scanner");
}
 
 
void loop()
{
  byte error, address;
  int nDevices;
 
  Serial.println("Scanning...");
 
  nDevices = 0;
  for(address = 16; address < 100; address++ )
  {
    // The i2c_scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address.
    delay(1);
    Wire.beginTransmission(address);
    error = Wire.endTransmission();
 
    if (error == 0)
    {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");
 
      nDevices++;
    }
    else if (error==4)
    {
      Serial.print("Unknown error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address,HEX);
    }    
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");


// // read MCP9800s data
// for(int i=0; i<1; i++){
//  MCP9800 = MCP9800s[i];
//  Serial.print("MCP9800 on adress ");
//  Serial.print(MCP9800,HEX);
//  Wire.beginTransmission(MCP9800);
//  Wire.write(MCP9800_TEMP);
//  Wire.endTransmission();
//  delayMicroseconds(50); 
//  Wire.requestFrom(MCP9800, 2); //read 2 bytes
//  uint16_t data = Wire.read()<<8;
//  data |= Wire.read();
//  delay(1);
//  Ts[i] = 0.0625*(data/16); // convert in °C
//  Serial.print(" T[°C]= ");
//  Serial.println(Ts[i],2);
//  }


  Serial.print("Re-starting in ");
  for (int i=0; i<=5; i++){ // wait 5 seconds for next scan
    Serial.print(5-i);
    delay(1000);
  }
  Serial.print("\n");
}

