//Pin definition
int LED = 3;
int LED_minus = 2;
int A_p = 5;
int A_m = 6;
int B_p = 9;
int B_m = 10;

//Variables 
int D = 4; //Delay en ms
int steps = 8; //power of 2
int N = 2; //reppeat
int long loop_count = 0;

void setup() {
// GPIO set up

 pinMode(LED, OUTPUT);
 pinMode(LED_minus, OUTPUT);
 pinMode(A_p, OUTPUT);
 pinMode(A_m, OUTPUT);
 pinMode(B_p, OUTPUT);
 pinMode(B_m, OUTPUT);
 
 digitalWrite(LED, LOW);
 digitalWrite(LED_minus, LOW);
 digitalWrite(A_p, LOW);
 digitalWrite(A_m, LOW);
 digitalWrite(B_p, LOW);
 digitalWrite(B_m, LOW);
}

void loop() {

  loop_count = loop_count + 1;
  
  digitalWrite(LED, HIGH);
  digitalWrite(LED_minus, LOW);
 
  //1st axis +
  for (int n=0; n<=N; n++){
    digitalWrite(B_p, LOW);
    digitalWrite(B_m, LOW);
    digitalWrite(A_p, 0);
    digitalWrite(A_m, LOW);
    delay(D);
    for (int i=0; i < 256/steps; i++){
      analogWrite(A_p, i*steps);
      delay(D);
      }
    for (int i=0; i < 256/steps; i++){
      analogWrite(A_p, 255-i*steps);
      delay(D);
      }
  
    //1st axis -
    digitalWrite(A_p, LOW);
    digitalWrite(A_m, 0);
    delay(D);
    for (int i=0; i < 256/steps; i++){
      analogWrite(A_m, i*steps);
      delay(D);
      }
    for (int i=0; i < 256/steps; i++){
      analogWrite(A_m, 255-i*steps);
      delay(D);
      }
    } 

  digitalWrite(LED_minus, HIGH);
  digitalWrite(LED, LOW);
    
  //2nd axis +
  for (int n=0; n<=N; n++){
    digitalWrite(B_p, 0);
    digitalWrite(B_m, LOW);
    digitalWrite(A_p, LOW);
    digitalWrite(A_m, LOW);
    delay(D);
    for (int i=0; i < 256/steps; i++){
      analogWrite(B_p, i*steps);
      delay(D);
      }
    for (int i=0; i < 256/steps; i++){
      analogWrite(B_p, 255-i*steps);
      delay(D);
      }
      
    //2nd axis -
    digitalWrite(B_p, LOW);
    digitalWrite(B_m, 0);
    delay(D);
    for (int i=0; i < 256/steps; i++){
      analogWrite(B_m, i*steps);
      delay(D);
      }
    for (int i=0; i < 256/steps; i++){
      analogWrite(B_m, 255-i*steps);
      delay(D);
      }  
  }
}
