Info: Reading netlist file "C:\Users\UserPC\Documents\LIESSE_PCB\by Mej\KiCad PO\v1.net".

Info: Using references to match components and footprints.

Info: Checking netlist component footprint "C1:/5AC3736C:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C2:/5AC37415:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C3:/5AC3FC25:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C4:/5AC3AAB2:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C5:/5AC3AAF1:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C6:/5AC3AB43:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C7:/5AC3FF50:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C8:/5AC4870B:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C9:/5AC4531A:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C10:/5AC475E9:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C11:/5AC3C95F:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C12:/5AC44048:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C13:/5AC46966:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C14:/5AC45574:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "C15:/5AC43FF7:Capacitors_SMD:C_0805".

Info: Checking netlist component footprint "D1:/5AC44330:LEDs:LED_0805".

Info: Checking netlist component footprint "J1:/5A9EC2CB:Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm".

Info: Checking netlist component footprint "J2:/5A9EC085:Pin_Headers:Pin_Header_Straight_2x20_Pitch2.54mm".

Changing component "J2:/5A9EC085" pin "1" net name from "" to "Net-(J2-Pad1)".

Changing component "J2:/5A9EC085" pin "7" net name from "" to "Net-(J2-Pad7)".

Changing component "J2:/5A9EC085" pin "10" net name from "" to "Net-(J2-Pad10)".

Changing component "J2:/5A9EC085" pin "11" net name from "" to "Net-(J2-Pad11)".

Changing component "J2:/5A9EC085" pin "13" net name from "" to "Net-(J2-Pad13)".

Changing component "J2:/5A9EC085" pin "16" net name from "" to "Net-(J2-Pad16)".

Changing component "J2:/5A9EC085" pin "17" net name from "" to "Net-(J2-Pad17)".

Changing component "J2:/5A9EC085" pin "18" net name from "" to "Net-(J2-Pad18)".

Changing component "J2:/5A9EC085" pin "19" net name from "" to "Net-(J2-Pad19)".

Changing component "J2:/5A9EC085" pin "21" net name from "" to "Net-(J2-Pad21)".

Changing component "J2:/5A9EC085" pin "22" net name from "" to "Net-(J2-Pad22)".

Changing component "J2:/5A9EC085" pin "23" net name from "" to "Net-(J2-Pad23)".

Changing component "J2:/5A9EC085" pin "24" net name from "" to "Net-(J2-Pad24)".

Changing component "J2:/5A9EC085" pin "26" net name from "" to "Net-(J2-Pad26)".

Changing component "J2:/5A9EC085" pin "27" net name from "" to "Net-(J2-Pad27)".

Changing component "J2:/5A9EC085" pin "28" net name from "" to "Net-(J2-Pad28)".

Changing component "J2:/5A9EC085" pin "29" net name from "" to "Net-(J2-Pad29)".

Changing component "J2:/5A9EC085" pin "31" net name from "" to "Net-(J2-Pad31)".

Changing component "J2:/5A9EC085" pin "35" net name from "" to "Net-(J2-Pad35)".

Changing component "J2:/5A9EC085" pin "37" net name from "" to "Net-(J2-Pad37)".

Changing component "J2:/5A9EC085" pin "38" net name from "" to "Net-(J2-Pad38)".

Changing component "J2:/5A9EC085" pin "40" net name from "" to "Net-(J2-Pad40)".

Info: Checking netlist component footprint "J3:/5A9EC361:Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm".

Info: Checking netlist component footprint "J4:/5A9EC304:Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm".

Info: Checking netlist component footprint "J5:/5AC3C003:Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm".

Info: Checking netlist component footprint "J6:/5AC3C48F:Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm".

Info: Checking netlist component footprint "L1:/5AC3AB80:Inductors_SMD:L_0805".

Info: Checking netlist component footprint "R1:/5AC3FB98:Resistors_SMD:R_0805".

Info: Checking netlist component footprint "R2:/5AC492FA:Resistors_SMD:R_0805".

Info: Checking netlist component footprint "R3:/5AC47596:Resistors_SMD:R_0805".

Info: Checking netlist component footprint "U1:/5A9EC27E:Housings_SOIC:SOIC-20W_7.5x12.8mm_Pitch1.27mm".

Changing component "U1:/5A9EC27E" pin "6" net name from "" to "Net-(U1-Pad6)".

Info: Checking netlist component footprint "U2:/5A9EC1EC:Housings_SON:VSON-10-1EP_3x3mm_Pitch0.5mm_ThermalPad".

Changing component "U2:/5A9EC1EC" pin "6" net name from "" to "Net-(U2-Pad6)".

Changing component "U2:/5A9EC1EC" pin "7" net name from "" to "Net-(U2-Pad7)".

Info: Checking netlist component footprint "U3:/5A9EC22D:Housings_SSOP:TSSOP-24_4.4x7.8mm_Pitch0.65mm".

Changing component "U3:/5A9EC22D" pin "12" net name from "" to "Net-(U3-Pad12)".

Changing component "U3:/5A9EC22D" pin "13" net name from "" to "Net-(U3-Pad13)".

Changing component "U3:/5A9EC22D" pin "14" net name from "" to "Net-(U3-Pad14)".

Info: Checking netlist component footprint "U4:/5ACCB55F:Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm".

Info: Checking netlist component footprint "U5:/5ACCB60E:Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm".

Info: Checking netlist component footprint "U6:/5ACCB661:Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm".

Remove single pad net "Net-(J2-Pad1)" on "J2" pad '1'

Remove single pad net "Net-(J2-Pad10)" on "J2" pad '10'

Remove single pad net "Net-(J2-Pad11)" on "J2" pad '11'

Remove single pad net "Net-(J2-Pad13)" on "J2" pad '13'

Remove single pad net "Net-(J2-Pad16)" on "J2" pad '16'

Remove single pad net "Net-(J2-Pad17)" on "J2" pad '17'

Remove single pad net "Net-(J2-Pad18)" on "J2" pad '18'

Remove single pad net "Net-(J2-Pad19)" on "J2" pad '19'

Remove single pad net "Net-(J2-Pad21)" on "J2" pad '21'

Remove single pad net "Net-(J2-Pad22)" on "J2" pad '22'

Remove single pad net "Net-(J2-Pad23)" on "J2" pad '23'

Remove single pad net "Net-(J2-Pad24)" on "J2" pad '24'

Remove single pad net "Net-(J2-Pad26)" on "J2" pad '26'

Remove single pad net "Net-(J2-Pad27)" on "J2" pad '27'

Remove single pad net "Net-(J2-Pad28)" on "J2" pad '28'

Remove single pad net "Net-(J2-Pad29)" on "J2" pad '29'

Remove single pad net "Net-(J2-Pad31)" on "J2" pad '31'

Remove single pad net "Net-(J2-Pad35)" on "J2" pad '35'

Remove single pad net "Net-(J2-Pad37)" on "J2" pad '37'

Remove single pad net "Net-(J2-Pad38)" on "J2" pad '38'

Remove single pad net "Net-(J2-Pad40)" on "J2" pad '40'

Remove single pad net "Net-(J2-Pad7)" on "J2" pad '7'

Remove single pad net "Net-(U1-Pad6)" on "U1" pad '6'

Remove single pad net "Net-(U2-Pad6)" on "U2" pad '6'

Remove single pad net "Net-(U2-Pad7)" on "U2" pad '7'

Remove single pad net "Net-(U3-Pad12)" on "U3" pad '12'

Remove single pad net "Net-(U3-Pad13)" on "U3" pad '13'

Remove single pad net "Net-(U3-Pad14)" on "U3" pad '14'

Error: Component 'J5' pad '3' not found in footprint 'Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm'

Error: Component 'J6' pad '3' not found in footprint 'Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm'

Error: Component 'U2' pad '~' not found in footprint 'Housings_SON:VSON-10-1EP_3x3mm_Pitch0.5mm_ThermalPad'

