REPO du stage LIESSE qui a eu lieu à l'IOGS le 2, 3 et 4 Mai 2018
Détails des fichiers
=======


## Photos de l'atelier:

  * Dossier pictures/


## Présentations:

  * Review de la Frugalité en instrumentation scientifique: Science_frugale_by_Francois_Piuzzi.pdf
  * Rappels sur le fonctionnement des moteurs: Motor_byMej.pptx
  * Présentation du Raspberry Pi: RPI_byMej.pptx


## Exemple de code Arduino en branchement direct:

  * Moteur linéaire (tête de lecture CD): Software/focus_head_test/
  * Moteur pas-à-pas: Software/step_motor_test/
  * Moteur BLDC: Software/BLDC_motor_speed_test


## Détail sur la carte électronique de commande moteur:

  * Shematics & co: datasheet/PCB_V3/
  * Détail des circuits intégrés et de la communication I2C: Composant_v3.pptx
  * Projets KiCad: KiCad_v2/
  * Bill of Material: BOM_By_Mej.xlsx


## Exemple de code Arduino en branchement via la carte de commande moteur (communication I2C):

  * Software/DRV8830_check
  * Software/DRV10975_check
  * Software/DRV8830_Super_check
  * Software/DRV10975_Super_check
